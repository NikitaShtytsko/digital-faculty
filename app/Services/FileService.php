<?php

namespace App\Services;

use App\Models\Files\EquipmentFile;
use App\Models\Files\LaboratoryFile;
use App\Models\Files\NewsFile;
use App\Models\UserFile;
use App\Repositories\FileRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FileService
{
    private $ownerId;
    /**
     * @var FileRepository
     */
    public $fileRepository;

    public function __construct()
    {
        $this->fileRepository = new FileRepository();
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param mixed $ownerId
     */
    public function setOwnerId(int $ownerId): void
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @param $path
     * @return bool
     */
    public function deleteFile($path)
    {
        try {
            unlink(storage_path('app/' . $path));
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    private function storeFile($file)
    {
        $path = $file->store('public');
        $file->path = $path;
        $file->origin = $this->ownerId;

        return $file;
    }

    /**
     * @param int $userId
     * @return Builder|Model
     */
    public function findUserAvatar(int $userId)
    {
        return $this->fileRepository->findUserFile($userId);
    }

    /**
     * @param $file
     * @return Builder|Model
     */
    public function uploadUserFile($file)
    {
        return $this->fileRepository->uploadUserFile(
            $this->storeFile($file)
        );
    }

    /**
     * @param $file
     * @return Builder|Model
     */
    public function uploadNewsFile($file)
    {
        return $this->fileRepository->uploadNewsFile(
            $this->storeFile($file)
        );
    }

    /**
     * @param $file
     * @return Builder|Model
     */
    public function uploadLaboratoryFile($file)
    {
        return $this->fileRepository->uploadLaboratoryFile(
            $this->storeFile($file)
        );
    }

    /**
     * @param $file
     * @return Builder|Model
     */
    public function uploadEquipmentFile($file)
    {
        return $this->fileRepository->uploadEquipmentFile(
            $this->storeFile($file)
        );
    }

    /**
     * @param $file
     * @return Builder|Model
     */
    public function changeUserAvatar($file)
    {
        /** @var UserFile $oldFile */
        $oldFile = $this->fileRepository->findUserFile(Auth::id());
        if ($oldFile) {
            $this->deleteFile($oldFile->path);
            (new UserRepository())->deleteUserFile();
        }

        return $this->uploadUserFile($file, Auth::id());
    }

    /**
     * @return void
     */
    public function deleteLaboratoryFiles($laboratoryId)
    {
        /** @var LaboratoryFile $file */
        $files = $this->fileRepository->findLaboratoryFiles($laboratoryId);
        foreach ($files as $file) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::LABORATORY_FILE,
                [$file->id]
            );
        }
    }

    /**
     * @param $fileId
     * @return void
     */
    public function deleteLaboratoryFile($fileId)
    {
        /** @var UserFile $file */
        if ($file = $this->fileRepository->findLaboratoryFile($this->ownerId, $fileId)) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::LABORATORY_FILE,
                [$file->id]
            );
        }
    }


    public function deleteEquipmentFiles(int $equipmentId)
    {
        /** @var EquipmentFile $file */
        $files = $this->fileRepository->findEquipmentFiles($equipmentId);
        foreach ($files as $file) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::EQUIPMENT_FILE,
                [$file->id]
            );
        }
    }

    /**
     * @param $fileId
     * @return void
     */
    public function deleteEquipmentFile($fileId)
    {
        /** @var UserFile $file */
        if ($file = $this->fileRepository->findEquipmentFile($this->ownerId, $fileId)) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::EQUIPMENT_FILE,
                [$file->id]
            );
        }
    }

    /**
     * @param int $newsId
     * @return void
     */
    public function deleteNewsFiles(int $newsId)
    {
        /** @var NewsFile $file */
        $files = $this->fileRepository->findNewsFiles($newsId);

        foreach ($files as $file) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::NEWS_FILE,
                [$file->id]
            );
        }
    }

    /**
     * @param int $userId
     * @return void
     */
    public function deleteUserFile(int $userId)
    {
        /** @var UserFile $file */
        if ($file = $this->fileRepository->findUserFile($userId)) {
            $this->deleteFile($file->path);
            $this->fileRepository->deleteFileRecord(
                FileRepository::USER_FILE,
                [$file->id]
            );
        }
    }
}

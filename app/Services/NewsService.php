<?php

namespace App\Services;

use App\Models\News;
use App\Repositories\NewsRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class NewsService
{
    /**
     * @var NewsRepository
     */
    public $newsRepository;

    /**
     * @param NewsRepository $repository
     */
    public function __construct(NewsRepository $repository)
    {
        $this->newsRepository = $repository;
    }

    /**
     * @return Builder[]|Collection
     */
    public function getNewsList()
    {
        return $this->newsRepository->getNewsList();
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id)
    {
        return $this->newsRepository->getNewsById($id);
    }

    /**
     * @param array $data
     * @param $files
     * @return News
     */
    public function createNews(array $data, $files)
    {
        $news = $this->newsRepository->createNews($data);

        if (!empty($files)) {
            $fileService = new FileService();
            $fileService->setOwnerId($news->id);
            foreach ($files as $file){
                $fileService->uploadNewsFile($file);
            }
        }

        return $news;
    }

    public function delete(int $newsId)
    {
        (new FileService())->deleteNewsFiles($newsId);
        $this->newsRepository->delete($newsId);
    }
}

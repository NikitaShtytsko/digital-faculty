<?php

namespace App\Services;

use App\Http\Requests\deleteEquipmentsPhotosRequest;
use App\Models\Equipment;
use App\Repositories\EquipmentRepository;

class EquipmentService
{
    /**
     * @var EquipmentRepository
     */
    public $equipmentRepository;

    /**
     * @param EquipmentRepository $repository
     */
    public function __construct(EquipmentRepository $repository)
    {
        $this->equipmentRepository = $repository;
    }

    /**
     * @param array $all
     * @param array|null $files
     * @return Equipment
     */
    public function createEquipment(array $all, ?array $files)
    {
        $equipment = $this->equipmentRepository->createEquipment($all);

        if (!empty($files)) {
            $fileService = new FileService();
            $fileService->setOwnerId($equipment->id);
            foreach ($files as $file) {
                $fileService->uploadEquipmentFile($file);
            }
        }

        return $equipment;
    }


    public function getEquipmentById(int $equipmentId)
    {
        return $this->equipmentRepository->findById($equipmentId);
    }

    /**
     * @param int $equipmentId
     * @param array $all
     * @return Equipment
     */
    public function editEquipment(int $equipmentId, array $all)
    {
        return $this->equipmentRepository->editEquipment($equipmentId, $all);
    }

    /**
     * @param int $equipmentId
     * @return void
     */
    public function deleteEquipment(int $equipmentId)
    {
        (new FileService())->deleteEquipmentFiles($equipmentId);
        $this->equipmentRepository->deleteEquipment($equipmentId);
    }

    /**
     * @param $equipmentId
     * @param array|null $files
     * @return void
     */
    public function addEquipmentPhotos($equipmentId, ?array $files)
    {
        if (empty($files))
            return;

        $fileService = new FileService();
        $fileService->setOwnerId($equipmentId);
        foreach ($files as $file) {
            $fileService->uploadEquipmentFile($file);
        }
    }

    /**
     * @param deleteEquipmentsPhotosRequest $request
     * @return void
     */
    public function deleteEquipmentPhotos(deleteEquipmentsPhotosRequest $request)
    {

    }
}

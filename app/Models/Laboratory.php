<?php

namespace App\Models;

use App\Models\Files\LaboratoryFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property string $name
 * @property string $contacts
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Equipment[] $equipments
 * @property LaboratoryFile[] $photos
 * @property LaboratorySchedule[] $schedules
 *
 *  * Class News
 * @package App\Models
 */
class Laboratory extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'contacts',
        'description',
    ];

    /**
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany(LaboratoryFile::class, 'laboratory_id');
    }

    /**
     * @return HasMany
     */
    public function equipments()
    {
        return $this->hasMany(Equipment::class, 'laboratory_id');
    }

    /**
     * @return HasMany
     */
    public function schedules()
    {
        return $this->hasMany(LaboratorySchedule::class, 'laboratory_id');
    }
}

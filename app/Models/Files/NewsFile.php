<?php

namespace App\Models\Files;

use App\Models\News;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * @property integer $id
 * @property integer $news_id
 * @property string $name
 * @property string $path
 * @property integer $size
 * @property string $created_at
 * @property string $updated_at
 * @property string $url
 *
 * @property News $news
 */
class NewsFile extends Model
{

    use HasFactory;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['news_id', 'name', 'path', 'size', 'created_at', 'updated_at'];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        return Storage::url($this->path);
    }

    /**
     * @return BelongsTo
     */
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}

<?php

namespace App\Models;

use App\Models\Files\NewsFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property boolean $is_visible
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property NewsFile[] $newsFiles
 *
 *  * Class News
 * @package App\Models
 */
class News extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function newsFiles()
    {
        return $this->hasMany(NewsFile::class, 'news_id', 'id');
    }
}

<?php

namespace App\Models;

use App\Models\Files\LaboratoryFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property integer $laboratory_id
 * @property integer $equipment_id
 * @property integer $user_id
 * @property string $description
 * @property string $date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Equipment $equipment
 * @property Laboratory $laboratory
 *
 *  * Class News
 * @package App\Models
 */
class LaboratoryRequest extends Model
{
    const STATUS_OPENED = 'opened';
    const STATUS_REJECTED = 'rejected';
    const STATUS_APPROVED = 'approved';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'description',
        'laboratory_id',
        'equipment_id',
        'date'
        ];

    /**
     * @return BelongsTo
     */
    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }

    /**
     * @return BelongsTo
     */
    public function laboratory()
    {
        return $this->belongsTo(Laboratory::class, 'laboratory_id');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeExactLaboratory($laboratoryId){
        return $this->where('laboratory_id', '=', $laboratoryId);
    }
}

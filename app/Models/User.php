<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property integer $id
 * @property integer $certificate
 * @property string $phone
 * @property string $email
 * @property string $role
 * @property string $email_verified_at
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $token
 * @property UserFile $avatar
 *
 *  * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ADMIN = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'certificate',
        'phone',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'id', 'user_id');
    }

    /**
     * @return HasOne
     */
    public function avatar()
    {
        return $this->hasOne(UserFile::class);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === self::ADMIN;
    }

    /**
     * @return bool
     */
    public function scopeWithProfile($query)
    {
        return $query->join('profiles', 'profiles.user_id', '=', 'users.id');
    }
}

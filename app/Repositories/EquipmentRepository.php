<?php

namespace App\Repositories;

use App\Models\Equipment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EquipmentRepository
{
    public function createEquipment(array $all)
    {
        $equipmentModel = new Equipment();
        $equipmentModel->laboratory_id = $all['laboratory_id'];
        $equipmentModel->fill($all)->save();

        return $equipmentModel;
    }

    /**
     * @param int $equipmentId
     * @param array $all
     * @return Equipment
     */
    public function editEquipment(int $equipmentId, array $all)
    {
        /** @var Equipment $equipmentModel */
        $equipmentModel = Equipment::query()
            ->where('id', '=', $equipmentId)
            ->first();

        if (isset($all['laboratory_id']))
            $equipmentModel->laboratory_id = $all['laboratory_id'];

        if (isset($all['description']))
            $equipmentModel->laboratory_id = $all['description'];

        $equipmentModel->save();
        return $equipmentModel->refresh();
    }

    public function deleteEquipment(int $equipmentId)
    {
        Equipment::query()
            ->where('id', '=', $equipmentId)
            ->delete();
    }

    public function findById(int $equipmentId)
    {
        return Equipment::query()
            ->findOrFail($equipmentId);
    }
}

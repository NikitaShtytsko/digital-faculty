<?php

namespace App\Repositories;

use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileRepository
{
    public function newProfile($data)
    {
        $profile = new Profile();
        $profile->fill($data);

        $profile->save();
    }

    public function update(array $data)
    {
        $profile = Profile::query()->find(Auth::id());

        $profile->fill($data);
        $profile->save();
    }
}

<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\UserFile;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Throwable;

class UserRepository
{
    /**
     * @param $email
     * @return Builder|Model
     */
    public function findUserByEmail($email)
    {
        return User::query()
            ->where('email', $email)
            ->firstOrFail();
    }

    /**
     * @throws Exception|Throwable
     */
    public function createNewUser(array $data): User
    {
        $user = new User();
        $user->fill($data);
        $user->password = Hash::make($user->password);
        $user->saveOrFail();

        return $user;
    }

    /**
     * @return mixed
     */
    public function getUsersList()
    {
        return User::query()
            ->withProfile()
            ->get();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getUserById(int $id)
    {
        return User::query()
            ->withProfile()
            ->findOrFail($id);
    }

    /**
     * @param array $data
     * @return void
     */
    public function update(array $data)
    {
        $user = User::query()->find(Auth::id());

        $user->fill($data);
        $user->save();
    }

    /**
     * @return void
     */
    public function deleteUserFile()
    {
        UserFile::query()
            ->where('user_id', Auth::id())
            ->delete();
    }
}

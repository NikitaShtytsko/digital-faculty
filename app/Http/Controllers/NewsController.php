<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Resources\NewsResource;
use App\Http\Resources\UserWithProfileResource;
use App\Services\NewsService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @var NewsService
     */
    public $newsService;

    /**
     * @param NewsService $service
     */
    public function __construct(NewsService $service)
    {
        $this->newsService = $service;
    }

    /**
     * @return NewsResource
     */
    public function newNews(CreateNewsRequest $request)
    {
        return new NewsResource(
            $this->newsService->createNews($request->all(), $request->file('files'))
        );
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function list()
    {
        return NewsResource::collection(
            $this->newsService->getNewsList()
        );
    }

    /**
     * @return NewsResource
     */
    public function getById(int $id)
    {
        return new NewsResource(
            $this->newsService->getById($id)
        );
    }

    /**
     * @return void
     */
    public function delete(int $newsId)
    {
        $this->newsService->delete($newsId);
    }
}

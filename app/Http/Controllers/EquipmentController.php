<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPhotosRequest;
use App\Http\Requests\CreateEquipmentRequest;
use App\Http\Requests\deleteEquipmentsPhotosRequest;
use App\Http\Requests\EditEquipmentRequest;
use App\Http\Resources\EquipmentResource;
use App\Services\EquipmentService;
use App\Services\FileService;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class EquipmentController extends Controller
{
    /**
     * @var EquipmentService
     */
    public $equipmentService;

    /**
     * @param EquipmentService $service
     */
    public function __construct(EquipmentService $service)
    {
        $this->equipmentService = $service;
    }

    /**
     * @param CreateEquipmentRequest $request
     * @return EquipmentResource
     */
    public function newEquipment(CreateEquipmentRequest $request)
    {
        return new EquipmentResource(
            $this->equipmentService->createEquipment($request->all(), $request->file('files')),
        );
    }

    /**
     * @param $id
     * @return EquipmentResource
     */
    public function getEquipmentById($id)
    {
        return new EquipmentResource(
            $this->equipmentService->getEquipmentById($id),
        );
    }

    /**
     * @param $id
     * @param EditEquipmentRequest $request
     * @return EquipmentResource
     */
    public function editEquipment($id, EditEquipmentRequest $request)
    {
        return new EquipmentResource(
            $this->equipmentService->editEquipment($id, $request->all()),
        );
    }

    /**
     * @param $id
     * @return void
     */
    public function deleteEquipment($id)
    {
        $this->equipmentService->deleteEquipment($id);
    }

    /**
     * @param $id
     * @param AddPhotosRequest $request
     * @return void
     */
    public function addEquipmentPhotos($id, AddPhotosRequest $request)
    {
        $this->equipmentService->addEquipmentPhotos($id, $request->file('files'));
    }
}

<?php

namespace App\Http\Controllers;


use App\Http\Requests\AddPhotosRequest;
use App\Http\Requests\CreateLaboratoryRequest;
use App\Http\Requests\EditLaboratoryRequest;
use App\Http\Requests\LaboratoryRequestDecisionRequest;
use App\Http\Requests\LaboratoryRequestNewRequest;
use App\Http\Requests\LaboratoryRequestsList;
use App\Http\Resources\LaboratoryRequestResource;
use App\Http\Resources\LaboratoryResource;
use App\Http\Resources\LaboratorySchedulesResource;
use App\Http\Resources\LaboratoryWithSchedulesResource;
use App\Models\LaboratoryRequest;
use App\Services\LaboratoryRequestService;
use App\Services\LaboratoryService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class LaboratoryController extends Controller
{
    /**
     * @var LaboratoryService
     */
    public $laboratoryService;

    /**
     * @param LaboratoryService $service
     */
    public function __construct(LaboratoryService $service)
    {
        $this->laboratoryService = $service;
    }

    /**
     * @param CreateLaboratoryRequest $request
     * @return LaboratoryResource
     */
    public function newLaboratory(CreateLaboratoryRequest $request)
    {
        return new LaboratoryResource(
            $this->laboratoryService->createLaboratory($request->all(), $request->file('files'))
        );
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getLaboratoryList()
    {
        return LaboratoryResource::collection(
            $this->laboratoryService->getLaboratoryList()
        );
    }

    /**
     * @param int $id
     * @return LaboratoryResource
     */
    public function getLaboratory(int $id)
    {
        return new LaboratoryResource(
            $this->laboratoryService->getLaboratory($id)
        );
    }

    /**
     * @param int $id
     * @return LaboratoryResource
     */
    public function editLaboratory(int $id, EditLaboratoryRequest $request)
    {
        return new LaboratoryResource(
            $this->laboratoryService->editLaboratory($id, $request->all())
        );
    }

    /**
     * @param int $id
     * @return void
     */
    public function deleteLaboratory(int $id)
    {
        $this->laboratoryService->deleteLaboratory($id);
    }

    /**
     * @param int $id
     * @return void
     */
    public function addLaboratoryPhotos(int $id, AddPhotosRequest $request)
    {
        $this->laboratoryService->addLaboratoryPhotos($id, $request->file('files'));
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function listRequests(LaboratoryRequestsList $request)
    {
        return LaboratoryRequestResource::collection(
            (new LaboratoryRequestService())->list($request->all())
        );
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getRequest(int $id)
    {
        $result = (new LaboratoryRequestService())->getById($id);

        return [
          'request' => new LaboratoryRequestResource($result['request']),
          'laboratory' => new LaboratoryWithSchedulesResource($result['laboratory']),
        ];
    }

    /**
     * @param int $requestId
     * @param LaboratoryRequestDecisionRequest $request
     * @return void
     */
    public function requestDecision(int $requestId, LaboratoryRequestDecisionRequest $request)
    {
        (new LaboratoryRequestService())->makeDecision($requestId, $request->all());
    }

    /**
     * @param LaboratoryRequestNewRequest $request
     * @return LaboratoryRequest
     */
    public function makeRequest(LaboratoryRequestNewRequest $request)
    {
        return (new LaboratoryRequestService())->newRequest($request->all());
    }
}

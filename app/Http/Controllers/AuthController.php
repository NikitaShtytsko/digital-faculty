<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;
use App\Http\Resources\UserResource;
use App\Repositories\FileRepository;
use App\Services\FileService;
use App\Services\NewsService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @var NewsService
     */
    public $userService;

    /**
     * @param UserService $repository
     */
    public function __construct(UserService $repository)
    {
        $this->userService = $repository;
    }

    /**
     * @param SignInRequest $request
     * @return UserResource
     */
    public function signIn(SignInRequest $request)
    {
        return new UserResource(
            $this->userService->loginUser($request->all())
        );
    }

    /**
     * @param SignUpRequest $request
     * @return UserResource
     * @throws \Throwable
     */
    public function signUp(SignUpRequest $request)
    {
        $user = $this->userService->registerNewUser($request->all());

        if ($file = $request->file('file')) {
            $fileService = new FileService();
            $fileService->setOwnerId($user->id);
            $fileService->uploadUserFile($file);
        }

        return new UserResource($user);
    }
}

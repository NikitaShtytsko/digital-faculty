<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        return [
            'email' => $this->email,
            'phone' => $this->phone,
            'token' => $this->token ?? null,
            'is_email_verify' => (boolean)$this->email_verified_at ?? false,
            'avatar' => $this->avatar->url ?? null,
        ];
    }
}

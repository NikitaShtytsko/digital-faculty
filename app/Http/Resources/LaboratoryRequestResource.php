<?php

namespace App\Http\Resources;

use App\Models\Equipment;
use App\Models\Laboratory;
use App\Models\LaboratoryRequest;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class LaboratoryRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var LaboratoryRequest $this */
        return [
            'id' => $this->id,
            'description' => $this->description,
            'status' => $this->status,
            'date' => $this->date,
            'laboratory' => new LaboratoryForRequestsResource($this->laboratory),
            'equipment' => new EquipmentResource($this->equipment),
            'user' => new UserWithProfileResource($this->user),
        ];
    }
}

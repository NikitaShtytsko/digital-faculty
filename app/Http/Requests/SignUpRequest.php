<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fistName' => 'nullable|string',
            'lastName' => 'nullable|string',
            'certificate' => 'required|unique:users,certificate',
            'password' => 'required|string',
            'phone' => 'required|string|unique:users,phone',
            'email' => 'required|string|unique:users,email',
            'file' => 'nullable|file',
            'sex' => 'required|string',
            'position' => 'nullable|string',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'phone' => 'Номер телефона',
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
        ];
    }
}
